#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Item {
    char name[50];
    struct Item *next;
} Item;

typedef struct List {
    char name[50];
    struct List *next;
    struct List *prev;
    Item *first_item;
} List;

List* board() {
    List *list1 = malloc(sizeof(List));
    strcpy(list1->name, "Cole");
    list1->next = NULL;
    list1->prev = NULL;
    Item *item1_1 = malloc(sizeof(Item));
    strcpy(item1_1->name, "Oculus Pro");
    Item *item1_2 = malloc(sizeof(Item));
    strcpy(item1_2->name, "Oculus Quest 1");
    item1_2->next = NULL;
    list1->first_item = item1_1;
    item1_1->next = item1_2;

    List *list2 = malloc(sizeof(List));
    strcpy(list2->name, "Danny");
    list2->next = NULL;
    list2->prev = list1;
    Item *item2_1 = malloc(sizeof(Item));
    strcpy(item2_1->name, "Oculus Quest 1");
    item2_1->next = NULL;
    Item *item2_2 = malloc(sizeof(Item));
    strcpy(item2_2->name, "3070 RTX");
    item2_2->next = NULL;
    list2->first_item = item2_1;
    item2_1->next = item2_2;

    List *list3 = malloc(sizeof(List));
    strcpy(list3->name, "Ben");
    list3->next = list2;
    list3->prev = NULL;
    Item *item3_1 = malloc(sizeof(Item));
    strcpy(item3_1->name, "Oculus Quest 2");
    item3_1->next = NULL;
    list3->first_item = item3_1;

    List *list4 = malloc(sizeof(List));
    strcpy(list4->name, "Darragh");
    list4->next = list3;
    list4->prev = list2;
    Item *item4_1 = malloc(sizeof(Item));
    strcpy(item4_1->name, "3070 RTX");
    item4_1->next = NULL;
    list4->first_item = item4_1;

    // Update prev pointers
    list1->prev = list2;
    list2->prev = list3;
    list3->prev = list4;

    // Set the board pointer
    List *board = list4;

    return board;
}

void displayBoard(List *board) {
    List *currentList = board; // Start from the tail of the board

    while (currentList != NULL) {
        printf("%s\n", currentList->name); // Print the list name

        Item *currentItem = currentList->first_item; // Start from the first item

        while (currentItem != NULL) {
            printf("\t%s\n", currentItem->name); // Print the item name
            currentItem = currentItem->next; // Move to the next item
        }

        printf("\n");
        currentList = currentList->next; // Move to the previous list
    }
}

void editBoard(List *board) {
    int choice;
    printf("Choose what you want to edit:\n");
    printf("1. List name\n");
    printf("2. Item\n");
    scanf("%d", &choice);

    if (choice == 1) {
        char listName[50];
        printf("Enter the name of the list you want to edit: ");
        scanf(" %[^\n]s", listName); // Read the list name with spaces

        List *currentList = board;
        while (currentList != NULL) {
            if (strcmp(currentList->name, listName) == 0) {
                printf("Enter the new name for the list: ");
                scanf(" %[^\n]s", currentList->name); // Read the new list name with spaces
                printf("List name updated successfully.\n");
                return;
            }
            currentList = currentList->next;
        }
        printf("List not found.\n");
    } else if (choice == 2) {
        char listName[50];
        printf("Enter the name of the list containing the item you want to edit: ");
        scanf(" %[^\n]s", listName); // Read the list name with spaces

        List *currentList = board;
        while (currentList != NULL) {
            if (strcmp(currentList->name, listName) == 0) {
                char itemName[50];
                printf("Enter the name of the item you want to edit: ");
                scanf(" %[^\n]s", itemName); // Read the item name with spaces

                Item *currentItem = currentList->first_item;
                while (currentItem != NULL) {
                    if (strcmp(currentItem->name, itemName) == 0) {
                        printf("Enter the new name for the item: ");
                        scanf(" %[^\n]s", currentItem->name); // Read the new item name with spaces
                        printf("Item name updated successfully.\n");
                        return;
                    }
                    currentItem = currentItem->next;
                }
                printf("Item not found in the list.\n");
                return;
            }
            currentList = currentList->next;
        }
        printf("List not found.\n");
    } else {
        printf("Invalid choice.\n");
    }
}


void saveBoardToFile(List *board, const char *filename) {
    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("Error opening file for writing.\n");
        return;
    }

    List *currentList = board;
    while (currentList != NULL) {
        fprintf(fp, "%s\n", currentList->name); // Write list name to file

        Item *currentItem = currentList->first_item;
        while (currentItem != NULL) {
            fprintf(fp, "\t%s\n", currentItem->name); // Write item name to file
            currentItem = currentItem->next;
        }

        currentList = currentList->next;
    }

    fclose(fp);
    printf("Changes saved to %s successfully.\n", filename);
}






int main(void) {
    List *boardPtr = board(); // Generate the board and store its pointer
    int userInput;
    FILE *fp;
    char myString[80];
    int displayBoardRequested = 0; // Flag to track if display board is requested

    while (1) { // Loop indefinitely until user chooses to quit
        printf("Menu:\n1. Display board\n2. Load board from a file\n3. Edit Board\n4. Save board to a file\n5. Quit\n");
        printf("Enter your choice (1-6): ");
        scanf("%d", &userInput);

        if ((userInput < 1) || (userInput > 6)) {
            printf("\nInvalid input, try again!\n");
            continue; // Restart the loop if the input is invalid
        }

        switch (userInput) {
            case 1:
                if (!displayBoardRequested) { // Check if display board is already requested
                    displayBoard(boardPtr);
                    displayBoardRequested = 1; // Set the flag to indicate display board is requested
                } else {
                    printf("Board is already displayed.\n");
                }
                break;
            case 2:
                fp = fopen("displayBoard.txt","r");
                if (fp == NULL) {
                    printf("Error opening file!\n");
                } else {
                    fgets(myString, 80, fp);
                    printf("%s", myString);
                    fclose(fp);
                }
                break;
            case 3:
                editBoard(boardPtr); // Call editBoard function
                displayBoardRequested = 0; // Reset display flag to ensure the updated board is displayed
                break;
            case 4:
                saveBoardToFile(boardPtr, "displayBoard.txt"); // Save board to file
                break;
            case 5:
                printf("Exiting program.\n");
                return 0; // Quit the program
            default:
                printf("Option not yet implemented.\n");
                break;
        }
    }

    return 0;
}
